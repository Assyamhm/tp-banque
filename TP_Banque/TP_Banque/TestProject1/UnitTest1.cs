using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;
namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testdebiter()
        {

            Compte c = new Compte(12345, "toto", 1200, -500);
            double montant = 2000;
            bool ok;
            ok = c.debiter(montant);
            Assert.AreEqual(c.Solde, 1200);

        }
        [TestMethod]
        public void Testsuperieur()
        {
            Compte c1 = new Compte(12345, "toto", 1200, -500);
            Compte c2 = new Compte(12347, "tata", 1400, -400);
            bool ok;
            ok = c1.superieur(c2);
            Assert.AreEqual(ok, false);
        }
        [TestMethod]
        public void Testtransferer()
        { 
            Compte c1 = new Compte(12345, "toto", 1200, -500);
            Compte c2 = new Compte(12348, "titi", 3300, -500);
            bool ok;
            double montant = 100;
            ok = c1.Transferer(montant, c2);
            Assert.AreEqual(ok, true);
            Assert.AreEqual(c1.Solde, 1100);
            Assert.AreEqual(c2.Solde, 3400);

        }


    }
}
